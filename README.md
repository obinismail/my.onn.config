# README #

This module is for use with Openbravo ERP 3.0.
It contains generic requirements by my clients that otherwise requires changes to the core.

### What does this module do ###

* Deactivate and replace original General Ledger to use my.onn.Ledger modules. my.onn.ledger contains Excel friendly GL export report.
* Allow user to select zero quantity product in Goods Shipment/Goods Receipt.
* Enable multi UOM which was disabled by Openbravo.

### Installation ###

* Make sure you have Openbravo ERP installed. Refer to [Installation - Openbravo](http://wiki.openbravo.com/wiki/Installation)
* Git clone into your ERP module folder.
* Run 

```
#!bash

ant update.database compile.src smartbuild
```

### For futher information, you can contact me at ###

* onn.khairuddin@gmail.com